<?php
    include_once 'connection.php';
    include_once 'header.php';


?>

<input type="hidden" value="<?php echo $logged_userId; ?>" id="user_id" />
<section id="right-content" class="top-header">
  <header class="clearfix task-header">
      <div class="top-nav">
        <form id="allocationAdd">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-4">
                  <select id="allocated-designation" class="form-control selectpicker noBorder" >
                      <option designation-id="0">Select Designation</option>
                      <?php
                          $getUserQuery = "SELECT * FROM designation";
                          include_once 'connection.php';
                          $exec = mysqli_query($connect, $getUserQuery);
                          if($exec){
                              while($row = mysqli_fetch_assoc($exec)){
                                  echo "<option
                                          value='".$row['designation']."'
                                          data-tokens='".$row['designation']."'
                                          designation-id='".$row['id']."'>"
                                              .$row['designation'].
                                       "</option>";
                              }
                          }
                      ?>
                  </select>
                </div>
                <div class="col-md-4">
                  <select id="allocated-team-member" class="form-control noBorder" >

                  </select>
                </div>
                <div class="col-md-4">
                  <select id="allocated-project" class="selectpicker form-control noBorder" data-live-search="true" data-size="15" >
                      <option project-id="0"> Select Project </option>
                      <?php
                          $getProjectQuery = "select * from projects order by projectName";
                          $exec = mysqli_query($connect, $getProjectQuery);
                          if($exec){
                              while($row = mysqli_fetch_assoc($exec)){
                                  echo "<option
                                          project-id='".$row['id']."'
                                          data-tokens='".$row['projectName']."'>"
                                              .$row['projectName'].
                                       "</option>";
                              }
                          }
                      ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-3">
                <select id="allocated-time" class="noBorder form-control">
                  <option value="0"> Select Allocation</option>
                  <option value="25">25%</option>
                  <option value="50">50%</option>
                  <option value="75">75%</option>
                  <option value="100">100%</option>
                </select>
              </div>
              <div class="col-md-3">
                    <input type="text"  value="" class="noBorder form-control allocationdate" placeholder="Start Date"  id="duration-start-date" />
              </div>
              <div class="col-md-3">
                <input type="text"  value="" class="noBorder form-control allocationdate"  placeholder="End Date"  id="duration-end-date" />
              </div>
            <div class="col-md-3">
              <button class="btn btn-primary" id="allocate">Allocate</button>
            </div>
              </div>
            </div>
        </div>
        </div>
      </div>
    </header>
<div class="container-fluid content">


  <div class="row">
    <div class="col-sm-12">
      <div class="row">

        <div class="col-md-8 pull-right allocation-heading">



            <div class="row">

              <div class="block-two col-md-12">
                <div class="selected-project col-md-4 col-sm-4">
                  <h5>Project Name</h5>
                </div>
                <div class="large-screen col-md-6 col-sm-6">
                  <div class="no-margin">
                    <div class=" col-md-4">
                      <h5>Allocation</h5>
                    </div>
                    <div class=" col-md-4 col-sm-4">
                      <h5>Start Date</h5>
                    </div>
                    <div class=" col-md-4 col-sm-4">
                      <h5>End Date</h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>

        </div>
        <div class="clearfix"></div>

      </div>
    </div>

    <div class="clearfix"></div>

    <div class="col-sm-12">
        <div class="spacer-inner">
          <div id="user-allocation"></div><!--Allocation Data renders here-->
        </div>
    </div>

            </div>
       </div>
</section>

<!--EDIT Modal-->
<div class="modal fade modal-with-form" id="editAllocationDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header blue-background">
          <button type="button" class="close" data-dismiss="modal" data-id="" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit Allocaton</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="col-xs-12">
              <div class="form-group">
                <span>Project*</span>
                <select id="editAllocationProject" class="selectpicker form-control" data-live-search="true" data-size=5 size="8">
                  <?php
                    $getProjectQuery = "select * from projects order by projectName";
                    $exec = mysqli_query($connect, $getProjectQuery);
                    if($exec){
                      while($row = mysqli_fetch_assoc($exec)) {
                        echo "<option project-id='".$row['id']."' data-tokens='".$row['projectName']."'>".$row['projectName']."</option>";
                      }
                    }
                  ?>
                </select>
              </div>
            </div>
            <div class="col-xs-4">
              <div class="form-group">
                <span>Hours</span>
                <select id="editAllocatedTime" class="noBorder form-control">
                  <option value="0"> Select Allocation</option>
                  <option value="25">25%</option>
                  <option value="50">50%</option>
                  <option value="75">75%</option>
                  <option value="100">100%</option>
                </select>
              </div>
            </div>
            <div class="col-xs-4">
              <div class="form-group">
                <span>Date*</span>
                <input type="text" readonly='true' placeholder="" class="form-control dateSelect" id="edit-startDate" name="" />
              </div>
            </div>
            <div class="col-xs-4">
              <div class="form-group">
                <span>Date*</span>
                <input type="text" readonly='true' placeholder="" class="form-control dateSelect" id="edit-endDate" name="" />
              </div>
            </div>
            <div class="clearfix"></div>
          </form>
        </div>
        <div class="modal-footer">
          <p class="pull-left modalFormAlert"></p>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-default mod-save-button" id="updateallocation">Save changes</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!--EDIT modal Ends-->

<!--DELETE Modal-->
<div class="modal fade" id="deleteAllocationDetail" data-id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog modal-sm vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Delete Task </h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <div class="confirmation-box">
                <p>
                  Remove Allocation?
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="confirmDelAllocation btn btn-danger">Delete</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Delete modal Ends-->

<script src="js/allocation.js"></script>
<script src="js/edit-project-allocation.js"></script>
<?php include_once 'footer.php'; ?>
