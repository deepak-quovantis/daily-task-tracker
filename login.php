<?php include_once 'header.php'; ?>

<div class="login-background"></div>
<div class="wrapper-login">
    <div class="login-container shadow-effect">
        <div class="login-box">

            <div class="logo">
                <img src="images/logo_small.png" alt="">
            </div>

            <div class="login-box-heading">
                <h1>Sign In</h1>
            </div>
             
            <form class="login-form">

                <div class="form-group">                    
                    <label for="login-email">EMAIL ID</label>                 
                    <input type="email" class="form-control" id="login-email" placeholder="example@quovantis.com" required="required">
                </div>
             
                <div class="form-group">
                    <label for="login-password">PASSWORD</label>                   
                    <input type="password" class="form-control" id="login-password" placeholder="Password" required="required">
                </div>

                <div class="form-group">
                    <button type="submit"  id='sign-in' class="btn btn-default">Sign In</button>
                </div>

            </form>
            <div class="alert alert-danger" role="alert"></div>

        </div>

        <div class="image-box">
           <div class="overlay"></div>
           <img src="images/logo.png"  alt="">
        </div>

     </div>
</div>
 
 <script type="text/javascript" src="js/jquery-md5.js"></script>
 <!--JS for login feature-->
 <script src="js/login.js"></script>
<?php include_once 'footer.php'; ?>