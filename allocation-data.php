<?php
  include_once 'connection.php';
  header('Content-Type: application/json');
  $today = strtotime(Date('Y-m-d'));
  //echo $today;
  $query2 = "select PA.`id` as allocationid,U.id as userid,D.designation, PA.`allocation`,PA.startDate,PA.endDate,P.projectName,U.userName from product_allocation as PA
   JOIN projects as P on P.id = PA.fk_projectId JOIN users as U on U.id = PA.fk_userId JOIN designation as D on D.id = U.fk_designationId ORDER BY endDate DESC";
     $exec2 = mysqli_query($connect,$query2);


     $users = array();
     if($exec2)
     { $i=0;
       while($row2 = mysqli_fetch_assoc($exec2))
       {

         $users['allocation'][]= $row2;
         $endDate = strtotime($row2['endDate']);
         if($endDate < $today)
         {
           $users['allocation'][$i]['isExpired'] = true;
         }
         else
         {
            $daysLeft = $endDate - $today;
            $daysLeft = floor($daysLeft/(60 * 60 * 24));

            if($daysLeft < 7)
            {
              $users['allocation'][$i]['expiringSoon'] = true;
            }
         }
         $i++;
       }
     }
     else {
       echo "could not exec" .mysqli_error($connect);
     }
       echo json_encode($users);
?>
