<?php
    include_once('connection.php');
    header('Content-Type: application/json');
    $today = date('Y-m-d');
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $myuserId = mysqli_real_escape_string($connect,$_GET['logged_userId']);
    $query = "SELECT T.`id`, U.`userName`, T.`hours`, T.`minutes`, T.`date`, T.`taskDescription`,T.`fk_userId`,U.`role`,P.projectName AS 'projectName'
                FROM tasks AS T
                INNER JOIN users U
                ON T.fk_userId = U.id
                INNER JOIN projects P
                ON T.fk_projectId = P.id
                WHERE T.fk_userId = ".$myuserId."
                ORDER BY `T`.`date` DESC limit 100";

    $exec = mysqli_query($connect, $query);
    $dataArray = array();
    if($exec)
    {
      $hours_today = "";
      $mins_today = "";

      $hours_yesterday = "";
      $mins_yesterday = "";
      $i = 0;
      while($row = mysqli_fetch_assoc($exec))
      {
        $dataArray['worklog'][] = $row;

        //date format trimming to "mm/dd"
        $trimmedDate = explode('-',$row['date']);
        $trimmedDate = $trimmedDate[1]."/".$trimmedDate[2];

        $dataArray['worklog'][$i]['shortDate'] = $trimmedDate;
        if($row['date'] == $today)
        {
          $dataArray['worklog'][$i]['loggedToday'] = 'true';
          $hours_today+= intval($row['hours']);
          $mins_today+= intval($row['minutes']);
        }
        if($row['date'] == $yesterday)
        {
          $dataArray['worklog'][$i]['loggedYesterday'] = 'true';
          $hours_yesterday+= intval($row['hours']);
          $mins_yesterday+= intval($row['minutes']);
        }


        $i++;
      }
      $dataArray['logToday']['hours'] = $hours_today ;
      $dataArray['logToday']['minutes'] = $mins_today;

      $dataArray['logYesterday']['hours'] = $hours_yesterday ;
      $dataArray['logYesterday']['minutes'] = $mins_yesterday;
      echo json_encode($dataArray);
    }
?>
