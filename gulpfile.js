var gulp = require('gulp'),
browserSync = require('browser-sync'),
sass = require('gulp-ruby-sass');

gulp.task('sass', function() {
	return sass('sass/*.scss')

	.on('error', sass.logError)
	.pipe(gulp.dest('css/'));
});

gulp.task('watch', function(){
	gulp.watch('sass/*.scss' ,  ['sass']);
	gulp.watch('*.html').on('change' , browserSync.reload);
  // Other watchers
})

gulp.task('browser-sync', function() {
  browserSync({
      port: 8083,
      server: {
        baseDir: './'
      }
    });
});

gulp.task('default', ['browser-sync','watch','sass']);