<?php
    include_once('connection.php');
    header('Content-Type: application/json');

    //$today = date("Y-m-d");
    //$firstday = date("Y-m-01");

    //$today = mysqli_real_escape_string($connect,$_GET['today']);
    //$firstday = mysqli_real_escape_string($connect,$_GET['firstday']);

    //$previousmonthFirstday = mysqli_real_escape_string($connect,$_GET['previousmonthFirstday']);
    //$previousmonthLastday = mysqli_real_escape_string($connect,$_GET['previousmonthLastday']);

    $startDate = mysqli_real_escape_string($connect,$_GET['startDate']);
    $endDate = mysqli_real_escape_string($connect,$_GET['endDate']);
    $userId  =  mysqli_real_escape_string($connect,$_GET['userId']);
    
    $query = "SELECT U.userName, P.projectName, SUM(T.hours) as totalHours, T.minutes, D.Designation as userDesignation  FROM tasks as T INNER JOIN users as U on U.id = T.fk_userId INNER JOIN designation AS D ON U.fk_designationId = D.id INNER JOIN projects as P ON T.fk_projectId = P.id  WHERE T.fk_userId = '".$userId."' AND T.date BETWEEN '".$startDate."' and '".$endDate."' GROUP BY P.projectName";

    $exec = mysqli_query($connect, $query);

    if($exec){
        $str = "";
        while($row = mysqli_fetch_assoc($exec)){

            if($str != ''){
                $str.=",";
            }

            $str.='{"userName":"'.$row['userName'].'","projectName":"'.$row['projectName'].'","hours":"'.$row['totalHours'].'","minutes":"'.$row['minutes'].'","userDesignation":"'.$row['userDesignation'].'"}';
        }
        $str = '{"projectdetails":['.$str.']}';
        echo $str;
    }
?>
