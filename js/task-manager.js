$(document).ready(function() {
    //Get today's Date in mm-dd-yyyy
    var curDate = new Date();
    var date = curDate.getDate();
    var month = curDate.getMonth()+1;
    if(month<10)
    {
      month = "0"+month;
    }
    var year = curDate.getFullYear();
    localStorage.taskLogYear = curDate.getFullYear();
    //console.log("initial:"+localStorage.taskLogYear);
    var todayDate = year+"-"+month+"-"+date;
    var todayDateTrim = month+"/"+date;

    function getHighlightDates()
    {
      var logged_userId = $('#user_id').val();
      var highlightDayLimit = 30;
      var notaskDate = [];
      var taskdates = [];
      $.ajax({
        type:'get',
        url:'get-task-dates.php',
        data:{userid:logged_userId},
        success: function(data)
        {
          jQuery.each(data.dates, function(key,val){
            taskdates.push(val.date);
          })
          //console.log(taskdates);
          while(highlightDayLimit >= 1 )
          {
            var dateToday = new Date();
            var setdate = dateToday.setDate(dateToday.getDate()-highlightDayLimit);
            var taskDate = dateToday.getDate();
            var taskMonth = dateToday.getMonth()+1;
            var taskYear = dateToday.getFullYear();
            if(taskMonth<10)
            {
              taskMonth = "0" + taskMonth;
            }
            if(taskDate<10)
            {
              taskDate = "0"+taskDate;
            }
            var checkDate = taskYear+"-"+taskMonth+"-"+taskDate;

            //console.log(checkDate);
            if(jQuery.inArray(checkDate,taskdates)== -1)
            {
              notaskDate.push(checkDate);
            }
            highlightDayLimit --;
          }
          localStorage["notaskDate"] =  JSON.stringify(notaskDate);
           //console.log(localStorage["notaskDate"]);
           return notaskDate;
        }
      });
    }
      //getHighlightDates();

    //function to highlight dates, if data task is not filled for that day.
    //var availableDates = localStorage.notask;//['2017-01-10','2017-01-11','2017-01-12'];//
    // var availableDates = [];
    // availableDates = JSON.parse(localStorage["notaskDate"]);
    function highlightdates(mydate) {

      var availYear = mydate.getFullYear();
      var availMonth = mydate.getMonth()+1;
      var availDate = mydate.getDate();
      if(availMonth<10)
      {
        availMonth = "0" + availMonth;
      }
      if(availDate<10)
      {
        availDate = "0"+availDate;
      }
      availday = availYear+ "-" +availMonth+"-"+availDate;
      //console.log("avail: "+availday);
      if (jQuery.inArray(availday, availableDates) > -1) {
          return [true,"notasklogged",""];
      } else {
          return [true,"logged",""];
      }
    }



    $(".datepicker").datepicker({
        altField: "#datepicker",
        altFormat: "mm/dd",
        //beforeShowDay: highlightdates,
        maxDate: todayDate,
        prevText: "",
        nextText: "",
        monthNames: [ "Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec" ],
        dayNamesMin: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
        onChangeMonthYear:function(y, m, i)
          {
              //console.log(i.selectedYear);
              localStorage.taskLogYear = i.selectedYear;
              //console.log("local:"+localStorage.taskLogYear)
          }


    });

    $("#editdatepicker").datepicker({
        altField: "#editdatepicker",
        altFormat: "mm/dd",
        maxDate: todayDate,
        prevText: "",
        nextText: "",
        monthNames: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec" ],
        dayNamesMin: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
        onChangeMonthYear:function(y, m, i)
          {
              //console.log(i.selectedYear);
              localStorage.taskLogYear = i.selectedYear;
              //console.log("local:"+localStorage.taskLogYear)
          }


    });

    $("#duration-start-date").datepicker({
        dateFormat: "yy-mm-dd",
        onSelect: function(){
          var endDateSelect = $("#duration-end-date");
          var startDate = $(this).datepicker('getDate');

          endDateSelect.datepicker('option', 'minDate', startDate);
        }


    });
    $("#duration-end-date").datepicker({
        dateFormat: "yy-mm-dd",
    });

    $('.dateSelect').datepicker({
      dateFormat: "yy-mm-dd"
    })


    $('.log-time').keypress(function(e){
    	 //if the letter is not digit then block the textbox
	    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	        return false;
	    }
    });
  });
