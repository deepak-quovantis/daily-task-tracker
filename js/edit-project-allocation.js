
function editAllocRecord(id){//id= allocation id
  $('#editAllocationDetail').modal('show');
  console.log("Project Allocation ID: "+id);
  $.ajax({
    type:'POST',
    url:'edit-project-allocation.php',
    data:{editId:id},
    success:function(data){
      data = $.parseJSON(data);
      console.log(data);
      var projectId = $('select#editAllocationProject option[data-tokens="'+data.projectName+'"]').attr('project-id');
      $('button[data-id="editAllocationProject"] span.filter-option').attr('id',projectId);
        //console.log("edited project id: "+projectId);
      $('button[data-id="editAllocationProject"] span.filter-option').text(data.projectName);
      $('#editAllocatedTime option[value="'+data.allocation+'"]').prop('selected',true);
      $('#edit-startDate').val(data.startDate);
      $('#edit-endDate').val(data.endDate);
      // var projectId = $('#editAllocationProject option[data-tokens="'+data.projectName+'"]').attr('project-id');
        console.log("Project Id: "+projectId);
       //return false;
    }
  });

  //************* Update project select id **************
   function updateAllocProjectId()
     {
       var newId = $('#editAllocationProject option:selected').attr('project-id');
       $('button[data-id="editAllocationProject"] span.filter-option').attr('id',newId);
       console.log("New ID: "+newId);
     }
   $('#editAllocationProject').change(function()
     {
       updateAllocProjectId();
     });


  $('#updateallocation').click(function(){
    // console.log(id);
    // return false;
    updateAllocRecord(id);
    //console.log("cscdscs");
  });
}

function updateAllocRecord(id)
{
  $('#editAllocationDetail').modal('hide');
  var editAllProjectID = $('button[data-id="editAllocationProject"] span.filter-option').attr('id');
  var editAllPercent = $('#editAllocatedTime option:selected').attr('value');
  var editAllStart = $('#edit-startDate').val();
  var editAllEnd = $('#edit-endDate').val();

  console.log(id+", "+editAllProjectID+", "+editAllPercent+","+editAllStart+', '+editAllEnd);
  //return false;
  $.ajax({
    type:'POST',
    url:'update-allocation.php',
    data:{allocationId:id,projctId:editAllProjectID,percentage:editAllPercent,start:editAllStart,end:editAllEnd},
    sucsess:function(data){
      console.log("updated to: "+ data);
      get_project_allocation_data();
    },
    error: function(a,b,c){
             console.log(c);
    }
  })
}

function deleteAllocRecord(id)
{
  $('#deleteAllocationDetail').modal('show');

  $('.confirmDelAllocation').click(function(){
    $('#deleteAllocationDetail').modal('hide');
    $.ajax({
      type:'POST',
      url:'delete-allocation.php',
      data:{allocationId:id},
      success: function(data)
      {
        console.log(data);
        get_project_allocation_data();
      }
    });

  })
}

// $('a[data-target="#allocate-project"]').click(function(e){
//     e.preventDefault();
//     var editId = $(this).attr("title");
//     console.log(editId);
//     var data = {"editId":editId};
//     $.ajax({
//       url:'edit-project-allocation.php',
//       type:'POST',
//       data:{"editId":editId},
//       success:function(data){
//         //console.log(data);
//         data = $.parseJSON(data);
//         console.log(data);
//       },
//       error: function(){
//         debugger
//       }
//
//     })
//   });
