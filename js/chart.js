
jQuery(document).ready(function(){


  // Random-colors-programmatically
  var randomColor = (function(){
    var golden_ratio_conjugate = 0.618033988749895;
    var h = Math.random();

    var hslToRgb = function (h, s, l){
        var r, g, b;

        if(s == 0){
            r = g = b = l; // achromatic
        }else{
            function hue2rgb(p, q, t){
                if(t < 0) t += 1;
                if(t > 1) t -= 1;
                if(t < 1/6) return p + (q - p) * 6 * t;
                if(t < 1/2) return q;
                if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
                return p;
            }

            var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            var p = 2 * l - q;
            r = hue2rgb(p, q, h + 1/3);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1/3);
        }

        return '#'+Math.round(r * 255).toString(16)+Math.round(g * 255).toString(16)+Math.round(b * 255).toString(16);
    };

    return function(){
      h += golden_ratio_conjugate;
      h %= 1;
      return hslToRgb(h, 0.5, 0.60);
    };
  })();

  get_chart_details();

});

function get_chart_details(){
    var logged_user_id = $("input#user_id").val();
    //console.log(logged_user_id);

      $.ajax({
       url:'getChartDetails.php',
       type:'POST',
       dataType: "json",
       data:{logged_user_id:logged_user_id},
       async: false,
       success:function(data){
          /*console.log(data)*/
          //console.log("success");
          jQuery('.productChart').html('');
          populateChart(data);
       }
    });
}

function populateChart(graphData){
  // var margin = {top: 20, right: 20, bottom: 70, left: 40},
  //     width = 600 - margin.left - margin.right,
  //     height = 300 - margin.top - margin.bottom;

  var margin = {top: 20, right: 20, bottom: 70, left: 40},
      width = 300,
      height = 300 - margin.top - margin.bottom;

  var formatPercent = d3.format(".0%");

  // set the ranges
  var x = d3.scale.ordinal().rangeRoundBands([0, width], .05);
  var y = d3.scale.linear().range([height, 0]);

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom");

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      //.tickFormat(formatPercent);
      .ticks(10);

  var tip = d3.tip()
    .attr('class', 'd3-tip')
    .offset([-10, 0])
    .attr('opacity','1')
    .html(function(d) {
      return "<strong>Total Hours:</strong> <span style='color:red'>" + d.TotalHours + "</span>";
    })

  // add the SVG element
  var svg = d3.select(".productChart").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  svg.call(tip);

  // load the data
  //d3.json("data.json",
    //function draw(error, data) {
      // data.forEach(function(d) {
      //     d.Product = d.Product;
      //     d.TotalHours = +d.TotalHours;
      // });

      var data = JSON.parse(graphData.json);
      //console.log(data);

      data.forEach(function(d) {
          d.Product = d.Product;
          d.TotalHours = +d.TotalHours;
      });

      // scale the range of the data
      x.domain(data.map(function(d) { return d.Product; }));
      //y.domain([0, d3.max(data, function(d) { return d.TotalHours; })]);
      y.domain([0, d3.max(data, function(d) { return 160; })]);

      // X-axis
      svg.append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + height + ")")
          .call(xAxis)
          .selectAll("text")
          .style("text-anchor", "end")
          .attr("dx", "-.8em")
          .attr("dy", "-.55em")
          .attr("y", 25)
          .attr("x", 8)
          .attr("transform", "rotate(-45)" );

      // Y-axis
      svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(0)")
        .attr("y", -19)
        .attr("x", 38)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Total Hours");

      svg.selectAll("bar")
        .data(data)
        .enter().append("rect")
        .attr("class", "bar")
        .attr("x", function(d) { return x(d.Product); })
        .attr("width", x.rangeBand())
        .attr("y", function(d) { return y(d.TotalHours); })
        .attr("height", function(d) { return height - y(d.TotalHours); })
        //.style({fill: randomColor})
        .on('mouseover', tip.show)
        .on('mouseout', tip.hide);
  //});

  // function type(d) {
  //   d.TotalHours = +d.TotalHours;
  //   return d;
  // }
  }
