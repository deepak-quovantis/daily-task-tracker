$(document).ready(function(){
  /*Convert Minutes to Hours*/
  function conv_mins_to_hrs(hour,min){
    var totalTime;
    if(min>=60)
    {
      hour= parseInt(hour + (min/60));
      min = min % 60;
      //console.log(min+","+hour);
    }
    else{
      /*console.log(totalTime);*/
    }
    totalTime = hour + "h "+ min + "m";
    return totalTime;
  }

  //convert logged time (hours/min) in decimal value
  function convertToDecimal(hours,mins){
    var totalTime = 0;
    hours = hours || 0;
    mins  = mins || 0;
    totalTime+=parseFloat(hours)+parseFloat((mins/60));
    return totalTime.toFixed(2);
  }

  /* get individual project Details */
  function individual_hours(){
    var dataset = [];
    var newDate = new Date();
    var day = newDate.getDate();
    if(day < 10){day = "0"+day;}
    var month = newDate.getMonth()+1;
    if(month < 10){month = "0"+month;}
    var year = newDate.getFullYear();
    var today = year+"-"+month+"-"+day;
    var firstday = year+"-"+month+"-"+"01";
    var previousmonthFirstday = year+"-"+(month-1)+"-"+"01"
    var previousmonthLastday = year+"-"+(month-1)+"-"+31;

    var userId = $('#allocated-team-member option:selected').attr('user-id');
    var startDate = $('#individual-start-date').val();
    var endDate = $('#individual-end-date').val();

    if(startDate == "")
    {
      startDate = firstday;
    }
    if(endDate == "")
    {
      endDate = today;
    }

    console.log(userId+", "+startDate +", "+endDate);
    $.ajax({
      url:'get_individual_details.php',
      type:'GET',
      data:{userId:userId,startDate:startDate,endDate:endDate},
      success:function(data){
        console.log(data.projectdetails);
        var proj_name = new Array();
        var proj_total_hours = new Array();
        var proj_total_minutes = new Array();
        var proj_design_hours = new Array();
        var proj_design_minutes = new Array();
        var proj_dev_hours= new Array();
        var proj_dev_minutes = new Array();
        var proj_members = new Array();
        $.each(data.projectdetails,function(index, itemdata){
          // console.log(itemdata);
          if(proj_name.indexOf(itemdata.projectName) < 0){
            proj_name.push(itemdata.projectName);
            proj_total_hours.push(parseInt(itemdata.hours));
            proj_total_minutes.push(parseInt(itemdata.minutes));
            proj_members.push(itemdata.userName);
            //check if user's Designation is Designer, add in designer's hour/Minutes
            if(itemdata.userDesignation.includes("Designer")){
              proj_design_hours.push(parseInt(itemdata.hours));
              proj_design_minutes.push(parseInt(itemdata.minutes));
            }
            else {
              proj_design_hours.push(parseInt(0));
              proj_design_minutes.push(parseInt(0));
            }
            //check if user's Designation is Designer, add in designer's hour/Minutes ends
          }
          else{
            var arrayPosition = proj_name.indexOf(itemdata.projectName);
            proj_total_hours[arrayPosition]= parseInt(proj_total_hours[arrayPosition]) + parseInt(itemdata.hours);
            proj_total_minutes[arrayPosition] = parseInt(proj_total_minutes[arrayPosition]) + parseInt(itemdata.minutes);

            if(itemdata.userDesignation.includes("Designer")){
              proj_design_hours[arrayPosition] = parseInt(proj_design_hours[arrayPosition]) + parseInt(itemdata.hours);
              proj_design_minutes[arrayPosition] = parseInt(proj_design_minutes[arrayPosition]) + parseInt(itemdata.minutes);
            }

            if(!proj_members[arrayPosition].includes(itemdata.userName)){
              proj_members[arrayPosition]+=", "+itemdata.userName;
            }
          }
        });
        // console.log(proj_members);
        var arrLength = proj_name.length;
        var i = 0;
        while(i < arrLength){
          var dataset_row = [];
          dataset_row.push(proj_members[i]);
          dataset_row.push(proj_name[i]);
          dataset_row.push(conv_mins_to_hrs(proj_design_hours[i],proj_design_minutes[i]));
          dataset_row.push(conv_mins_to_hrs(proj_total_hours[i] -proj_design_hours[i],proj_total_minutes[i] - proj_design_minutes[i]));
          dataset_row.push(conv_mins_to_hrs(proj_total_hours[i],proj_total_minutes[i]));
          dataset.push(dataset_row);
          i++;
        }

        //$('#total-report').DataTable().destroy();
        $('#total-individual-report').DataTable({
          dom: 'Bfrtip',
          buttons: ['csv', 'excel', 'pdf', 'print'],
          data: dataset,
          columns:
          [
              { title: "Members" },
              { title: "Project" },
              { title: "Design Time" },

              { title: "Total Time" }
          ]
        });
      }, // END SUCCESS
      error: function (textStatus, errorThrown) {
        Success = false;//doesnt goes here
      }
    }); // END AJAX CALL
  }; // END FUNCTION individual_hours

  /* get individual project Details */
  function individual_project_hours(){
    var dataset = [];
    var newDate = new Date();
    var day = newDate.getDate();
    if(day < 10){day = "0"+day;}
    var month = newDate.getMonth()+1;
    if(month < 10){month = "0"+month;}
    var year = newDate.getFullYear();
    var today = year+"-"+month+"-"+day;
    var firstday = year+"-"+month+"-"+"01";
    var previousmonthFirstday = year+"-"+(month-1)+"-"+"01"
    var previousmonthLastday = year+"-"+(month-1)+"-"+31;

    var startDate = $('#individual-project-start-date').val();
    var endDate = $('#individual-project-end-date').val();
    if(startDate == "")
    {
      startDate = firstday;
    }
    if(endDate == "")
    {
      endDate = today;
    }

    console.log(startDate +", "+endDate);
    $.ajax({
      url:'get_project_details.php',
      type:'GET',
      data:{today:today,firstday:firstday,previousmonthLastday:previousmonthLastday,previousmonthFirstday:previousmonthFirstday,startDate:startDate,endDate:endDate},
      success:function(data){
        console.log(data.projectdetails);
        var proj_name = new Array();
        var proj_total_hours = new Array();
        var proj_total_minutes = new Array();
        var proj_design_hours = new Array();
        var proj_design_minutes = new Array();
        var proj_dev_hours= new Array();
        var proj_dev_minutes = new Array();
        var proj_members = new Array();
        $.each(data.projectdetails,function(index, itemdata){
          // console.log(itemdata);
          if(proj_name.indexOf(itemdata.projectName) < 0){
            proj_name.push(itemdata.projectName);
            proj_total_hours.push(parseInt(itemdata.hours));
            proj_total_minutes.push(parseInt(itemdata.minutes));
            proj_members.push(itemdata.userName);
            //check if user's Designation is Designer, add in designer's hour/Minutes
            if(itemdata.userDesignation.includes("Designer")){
              proj_design_hours.push(parseInt(itemdata.hours));
              proj_design_minutes.push(parseInt(itemdata.minutes));
            }
            else {
              proj_design_hours.push(parseInt(0));
              proj_design_minutes.push(parseInt(0));
            }
            //check if user's Designation is Designer, add in designer's hour/Minutes ends
          }
          else{
            var arrayPosition = proj_name.indexOf(itemdata.projectName);
            proj_total_hours[arrayPosition]= parseInt(proj_total_hours[arrayPosition]) + parseInt(itemdata.hours);
            proj_total_minutes[arrayPosition] = parseInt(proj_total_minutes[arrayPosition]) + parseInt(itemdata.minutes);

            if(itemdata.userDesignation.includes("Designer")){
              proj_design_hours[arrayPosition] = parseInt(proj_design_hours[arrayPosition]) + parseInt(itemdata.hours);
              proj_design_minutes[arrayPosition] = parseInt(proj_design_minutes[arrayPosition]) + parseInt(itemdata.minutes);
            }

            if(!proj_members[arrayPosition].includes(itemdata.userName)){
              proj_members[arrayPosition]+=", "+itemdata.userName;
            }
          }
        });
        // console.log(proj_members);
        var arrLength = proj_name.length;
        var i = 0;
        while(i < arrLength){
          var dataset_row = [];
          dataset_row.push(proj_name[i]);
          dataset_row.push(convertToDecimal(proj_total_hours[i],proj_total_minutes[i]));
          dataset_row.push(convertToDecimal(proj_design_hours[i],proj_design_minutes[i]));
          dataset_row.push(convertToDecimal(proj_total_hours[i] -proj_design_hours[i],proj_total_minutes[i] - proj_design_minutes[i]));
          dataset_row.push(proj_members[i]);
          dataset.push(dataset_row);
          i++;
        }

        //$('#total-report').DataTable().destroy();
        $('#total-project-report').DataTable({
          dom: 'Bfrtip',
          buttons: ['csv', 'excel', 'pdf', 'print'],
          data: dataset,
          columns:
          [
              { title: "Project" },
              { title: "Total Time" },
              { title: "Design Time" },
              
              { title: "Members" }
          ]
        });
      }, // END SUCCESS
      error: function (textStatus, errorThrown) {
        Success = false;//doesnt goes here
      }
    }); // END AJAX CALL
  }// END FUNCTION individual_project_hours



  $( ".individual-report-daterange" ).datepicker({
    dateFormat: "yy-mm-dd"
  });


  $('#filter-project-report').click(function(){
    individual_project_hours();
  });

  $('#filter-individual-report').click(function(){
    individual_hours();
  });
});
