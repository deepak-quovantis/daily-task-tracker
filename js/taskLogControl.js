// *************** control date format from (mm/dd) to be saved in DB (YYYY-MM-DD) ************
function alterDateFormat(myDate)
{
  var dateExplode = myDate.split("/");
  var fullDateTime = localStorage.taskLogYear+"-"+dateExplode[0]+"-"+dateExplode[1];
  return fullDateTime
}


// *********** control date format from (yyyy-mm-dd) to be saved in DB (MM/DD) ************
function cropDate(myDate)
{
  var dateExplode = myDate.split("-");
  var fullDateTime = dateExplode[1]+"/"+dateExplode[2];
  return fullDateTime
}


//************* edit task ************
function editTask(id)
{
  $('#editTaskDetail').modal('show');

  $.ajax({
    url:'edit-task.php',
    type:'POST',
    data:{taskId:id},
    success:function(data){
      data = $.parseJSON(data);
      console.log(data);
      $('#editTaskDetail .bootstrap-select button[data-id="editProjectName"] .filter-option').text(data["projectName"]);
      $('#editTaskDetail .bootstrap-select button[data-id="editProjectName"]').attr('title',data["projectName"]);
      $('#editTaskDetail .bootstrap-select button[data-id="editProjectName"] span.filter-option').attr('id',data["projectId"]);
      $('#edittxtHours').val(data["hours"]);
      $('#edittxtMin').val(data["minutes"]);
      $('#editTaskDetail #editdatepicker').val(cropDate(data["date"]));
      $('#edittxtDesc').val(data["taskDescription"]);
      //var editProjectID = $('select#editProjectName option[data-tokens="'+data['projectName']+'"]').attr('project-id');

      //Modal function
      $('#editTaskDetail').modal('show');
      $('#editTaskDetail').attr('data-id',id);
    }
  });
}


//************* Update project select id **************
 function updateProjectId()
   {
     var newId = $('#editProjectName option:selected').attr('project-id');
     $('#editTaskDetail .bootstrap-select button[data-id="editProjectName"] span.filter-option').attr('id',newId);
   }
 $('#editProjectName').change(function()
   {
     updateProjectId();
   });

/************ UPDATE FUNCTIONALITY HERE ***********/
$('#update-button').on('click',function(){
  var taskId = $('#editTaskDetail').attr('data-id');
  var timeHours = $('#edittxtHours').val();
  var timeMins = $('#edittxtMin').val();
  var timeDate = alterDateFormat($('#editTaskDetail #editdatepicker').val());
  var description = $('#edittxtDesc').val();
  var projectId = $('button[data-id="editProjectName"] span.filter-option').attr('id');
  console.log("project id: "+projectId);
  //return false;
  if(timeHours === ""){
    timeHours= 0;
  }

  if(description ==""){
    $('.modalFormAlert').text("Please enter description of task you did").removeClass('success-text').addClass('warning-text');
    return false;
  }

  if(timeHours==0 && timeMins == 0)
  {
    $('.modalFormAlert').text("Please fill the time spent on this task.").removeClass('success-text').addClass('warning-text');
    return false;
  }

  $('.modalFormAlert').text("Task Updated Successfully.").addClass('success-text').show().fadeOut(2000);

  $.ajax({
    url:'update-task.php',
    type:'POST',
    data:{
        taskId: taskId,
        timeHours: timeHours,
        timeMins: timeMins,
        timeDate: timeDate,
        description: description,
        projectId: projectId

    },
    success:function(data){
      //console.log(data);
      showtasklog();
      $('#editTaskDetail').modal('hide');
    }
  });
});
/* END UPDATE FUNCTIONALITY HERE */


//***********delete task***************
function deleteTask(id){
  //show modal
  $('#deleteTaskDetail').modal('show');

  $('#confirm-delete-data').on('click', function()
  {
    //hide modal
    $('#deleteTaskDetail').modal('hide');
    //delete data from DB
    $.ajax({
      url:'delete-task.php',
      type:'POST',
      data:{taskId:id},
      success:function(data)
        {
          showtasklog();
        }
    });
  });
};
