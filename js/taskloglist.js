function conv_mins_to_hrs(hour,min){
  var totalTime;
  if(min>=60)
  {
    hour= parseInt(hour) + parseInt(min/60);
    min = min % 60;
    //console.log("Hour: "+hour);
  }
  else{
    /*console.log(totalTime);*/
  }
  if(!hour)
  {
    hour = 0;
  }
  if(!min)
  {
    min = 0;
  }
  totalTime = hour + " h "+ min + " min";
  return(totalTime);
}

function showtasklog()
{
  var logged_userId = $('#user_id').val();
  //console.log(logged_userId);
  $.ajax({
    type:'GET',
    url:'tasklog.php',
    data:{logged_userId:logged_userId},
    success: function(data)
    {
      //console.log(data);
      $('#hoursToday').text("Today "+conv_mins_to_hrs(data.logToday.hours,data.logToday.minutes));
      $('#hoursYesterday').text("Yesterday "+conv_mins_to_hrs(data.logYesterday.hours,data.logYesterday.minutes));
      var data_container = $('#tasklog');
      var data_container_for_today = $('#tasklogToday');
      var data_container_for_yesterday = $('#tasklogYesterday');
      data_container.empty();
      data_container_for_today.empty();
      data_container_for_yesterday.empty();

      $.get('templates/taskloglist-tmpl.html',function(template, textStatus, jqXhr){
        data_container_for_today.append(Mustache.render($(template).filter('#taskloglisttoday').html(),data));
      });

      $.get('templates/taskloglist-tmpl.html',function(template, textStatus, jqXhr){
        data_container_for_yesterday.append(Mustache.render($(template).filter('#taskloglistyesterday').html(),data));
      });

      $.get('templates/taskloglist-tmpl.html',function(template, textStatus, jqXhr){
        data_container.append(Mustache.render($(template).filter('#taskloglist').html(),data));
      });
      //empty add task form
      $("#addtask").trigger("reset");
      $('.selectProject').selectpicker('refresh');

    },
    error : function(jqXHR, textStatus, errorThrown)
    {
      console.log(jqXHR+","+errorThrown);
    }
  });
};

function ind_hours_current_month(){
  var loggedId = $('#user_id').val();
  var date = new Date();
  var month = date.getMonth() + 1;
  if(month < 10){
    month = '0' + month;
  }
  var year = date.getFullYear();
  var day = date.getDay();
  if(day<10){
    day = "0"+day;
  }
  $.ajax({
    url:'ind_hours_current_month.php',
    type:'GET',
    data:{myId:loggedId},
    success:function(data){
      console.log(data);
      console.log(conv_mins_to_hrs(data.hours,data.minutes));
      $('#myHours span').text(conv_mins_to_hrs(data.hours,data.minutes));
    }
  });
};
