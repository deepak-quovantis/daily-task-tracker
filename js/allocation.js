var count = -1;

jQuery(document).ready(function(){

  //SORT USER BY DESIGNATION
  function sort_by_designation(id)
  {
    $.ajax({
      type:'GET',
      url:'sortById.php',
      data:{id:id},
      success: function(data)
      {
        //console.log(data);

        $('#allocated-team-member').html("");
        $.each(data,function(i,item){
          //console.log(item.userName);
          $('#allocated-team-member').append('<option user-id = "'+item.id+'">'+item.userName+'</option>');

        })
      },
      error: function(textStatus,errorThrown)
      {
        console.log(textStatus);
      }
    })
  }

  //get designation ID and filter users with same id.
  $('#allocated-designation').change(function(){
    var designationId = $('#allocated-designation option:selected').attr('designation-id');
    console.log(designationId);
    sort_by_designation(designationId);
  })


  //ADD Project Allocation
  $('#allocate').on('click',function(e){
    e.preventDefault();
    var al_designation = $('#allocated-designation option:selected').attr('designation-id');
    var al_member = $('#allocated-team-member option:selected').attr('user-id');
    var al_project = $('#allocated-project option:selected').attr('project-id');
    var allocation_val = $('#allocated-time option:selected').attr('value');
    var duration_start_date = $('#duration-start-date').val();
    var duration_end_date   =$('#duration-end-date').val();
    console.log(al_designation);
    if(al_designation == "0"){
      alert('Please select Designation');
      return false;
    }

    if(al_project == "0"){
      alert('Please Select Project');
      return false;
    }


    if(allocation_val == "0")
    {
      alert('allocation cant be 0');
      return false;
    }

    if(duration_start_date=="" )
    {
        alert("Start Date Can't be empty");
        return false;
    }

    if(duration_end_date < duration_start_date){
        alert('End date can\'t be earlier than start date');
        return false;
    }
    var projectId = $('#projectName option:selected').attr('project-id');
    var logged_user_id = $("input#user_id").val();
    $.ajax({
      url:'saveAllocation.php',
      type:'POST',
      data:{userName: al_member,
            userDesignation: al_designation,
            projectId: al_project,
            allocation: allocation_val,
            startDate: duration_start_date,
            endDate:   duration_end_date,
            },

      success:function(data){
        //console.log(data);
        if(data ==='0')
        {

          alert("duplicate data");
          return false;
        }
        else {

            //console.log(data);
            get_project_allocation_data();
        }

      }
    });

  });
  //ADD Allocation Ends
}); // JQuery document end

  function get_project_allocation_data()
  {
    $.ajax({
      url:"allocation-data.php",
      type:'GET',
      success:function(data)
      {
        //console.log("data fetched");
        var userId = [];
        var totalAllocation = [];
        $.each(data.allocation,function(key,value){
          //console.log(value);
          var datacontainer ="";
          if(jQuery.inArray(value.userid,userId) == -1)
          {
            userId.push(value.userid);
            $('#user-allocation').append('<div class="singleuser" id="'+value.userid+'"></div>');
            datacontainer = $('#'+value.userid);

            datacontainer.empty().append('<p class="col-md-4"><strong>'+value.userName+' </strong><br><small>('+value.designation+')</small></p>');

            if(!value.isExpired)
            {
              totalAllocation.push(parseInt(value.allocation));
            }

            else
            {
              totalAllocation.push(0);
            }

          }

          else
          {
            datacontainer = $('#'+value.userid);
            if(!value.isExpired)
              {
                totalAllocation[jQuery.inArray(value.userid,userId)]+= parseInt(value.allocation);
              }
            else
               {
                totalAllocation[jQuery.inArray(value.userid,userId)]+= parseInt(0);
               }
          }

           $.get('templates/user-allocation-detail.html',function(template, textStatus, jqXhr){
            datacontainer.append(Mustache.render($(template).filter('#allocation-detail').html(),value));
            });

        })

        var counter = 0;
        //console.log(userId);
        //console.log(totalAllocation.length);
        while(counter < totalAllocation.length)
        {
          //console.log(totalAllocation[counter]);
          if(totalAllocation[counter] > 100)
          {
            $('.singleuser:nth-child('+(counter+1)+')').addClass('overAllocated');
          }

          counter++;
        }
        $("#allocationAdd").trigger("reset");
        $('.selectProject').selectpicker('refresh');
      },
      error:function(a,b,c)
      {
        console.log(c);
      }
    });
  };

  get_project_allocation_data();
