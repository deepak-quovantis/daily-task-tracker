<?php
    include_once('connection.php');
    header('Content-Type: application/json');

    //$userName = mysqli_real_escape_string($connect,$_POST['userName']);
    $logged_user_id = mysqli_real_escape_string($connect,$_POST['logged_user_id']);

    //$query = "select sum(hours) as totalHours,sum(minutes) as totalMinutes,projectName from tasks where userName = '".$userName."' GROUP BY projectName"  ;

    //$query = "SELECT T.ProjectName AS projectName, SUM(T.Hours) AS totalHours, SUM(T.Minutes) AS totalMinutes FROM `tasks` AS T INNER JOIN users as U ON T.fk_userId = U.id INNER JOIN product_allocation AS P ON T.projectName = P.product WHERE U.userEmail = '".$userEmail."' GROUP BY T.projectName;";

    //$query = "SELECT T.ProjectName AS projectName, SUM(T.Hours) AS totalHours, SUM(T.Minutes) AS totalMinutes FROM `tasks` AS T INNER JOIN users as U ON T.fk_userId = U.id WHERE U.userEmail = '".$userEmail."' GROUP BY T.projectName;";

    $query = "SELECT DISTINCT projectName, IfNull(hours,0) as 'totalHours', IfNull(minutes,0) as 'totalMinutes' FROM 
              (Select Users.ID, Users.username, Users.useremail,pa.fk_projectId, 
              (SELECT SUM(hours) from tasks where tasks.fk_userId = users.id and tasks.fk_projectId = pa.fk_projectId) 'hours', 
              (SELECT SUM(minutes) from tasks where tasks.fk_userId = users.id and tasks.fk_projectId = pa.fk_projectId) 'minutes',
              (SELECT projects.projectName from projects WHERE projects.id= pa.fk_projectId) as projectName
              from users inner join product_allocation pa 
              where Users.id = pa.fk_userId 
            UNION 
              Select Users.ID, Users.username, Users.useremail, tasks.fk_projectId, SUM(tasks.hours), SUM(tasks.minutes),
              (SELECT projects.projectName from projects WHERE projects.id= tasks.fk_projectId) as projectName
              FROM users 
              INNER JOIN tasks 
              ON users.id = tasks.fk_userId 
              GROUP BY Users.ID, Users.username, Users.useremail, tasks.fk_projectId LIMIT 0, 100) c1 
            WHERE c1.ID = '".$logged_user_id."'";

    $exec = mysqli_query($connect, $query);
    if($exec){
        $json_output=[];
        while($row = mysqli_fetch_assoc($exec)){
            $totalHours= $row['totalHours'] + ($row['totalMinutes']/60);
            $json_output[]= [ 'Product' => $row['projectName'], 'TotalHours' => $totalHours ];
        }
        //file_put_contents('data.json', json_encode($json_output));
        $return["json"] = json_encode($json_output);
        echo json_encode($return);
    }
    else{
        echo "could not execute query ". mysqli_error($connect);
    }
?>