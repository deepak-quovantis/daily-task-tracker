<?php
    include_once 'connection.php';
?>
<?php
    include_once 'header.php';
?>
<!-- content section -->
<section id="right-content" class="top-header">
  

  <div class="container top-margin-due-header-fixed">
    <ul  class="nav nav-pills">
      <li class="active">
        <a href="#individual-project-detail" data-toggle="tab">Project-Report</a>
      </li>
      <li><a href="#individual-detail" data-toggle="tab">Individual-Report</a>
      </li>
    </ul>

    <div class="tab-content clearfix">
      <div class="tab-pane active" id="individual-project-detail">
         <!--Date range section-->
        <div class="row ind-proj-details">
          <div class="col-xs-12">
            <h3>Individual Project Report</h3>
            <div class="row">
              <div class="col-xs-4">
                <input type="text" id="individual-project-start-date" class="individual-report-daterange form-control">
              </div>
              <div class="col-xs-4">
                <input type="text" id="individual-project-end-date" class="individual-report-daterange form-control">
              </div>
              <div class="col-xs-4">
                <button type="submit" id="filter-project-report" class="btn btn-primary">Filter</button>
              </div>
            </div>
          </div>
        </div>
        <div class="spacer"></div>
          <!--Date range section ends-->
        <table  class="table" id="total-project-report" cellspacing="0" width="100%"></table>
      </div>
      <div class="tab-pane" id="individual-detail">
        <!--Date range section-->
        <div class="row ">
          <div class="col-xs-12">
            <h3>Individual  Report</h3>
            <div class="row">
              <div class="col-xs-4">
                <div class="row">
                  <div class="col-xs-6">
                    <select id="allocated-designation" class="form-control selectpicker" >
                          <option designation-id="0">Select Designation</option>
                          <?php
                              $getUserQuery = "SELECT * FROM designation";
                              include_once 'connection.php';
                              $exec = mysqli_query($connect, $getUserQuery);
                              if($exec){
                                  while($row = mysqli_fetch_assoc($exec)){
                                      echo "<option
                                              value='".$row['designation']."'
                                              data-tokens='".$row['designation']."'
                                              designation-id='".$row['id']."'>"
                                                  .$row['designation'].
                                           "</option>";
                                  }
                              }
                          ?>
                      </select>
                  </div>
                  <div class="col-xs-6">
                    <select id="allocated-team-member" class="form-control" >

                      </select>
                  </div>
                </div>
              </div>
              <div class="col-xs-8">
                <div class="row">
                  <div class="col-xs-4">
                    <input tproject-start-dateype="text" id="individual-start-date" class="individual-report-daterange form-control">
                  </div>
                  <div class="col-xs-4">
                    <input type="text" id="individual-end-date" class="individual-report-daterange form-control">
                  </div>
                  <div class="col-xs-4">
                    <button type="submit" id="filter-individual-report" class="btn btn-primary">Filter</button>
                  </div>
                </div>
              </div>
            </div>
              
          </div>
        </div>
    <div class="spacer"></div>
      <!--Date range section ends-->
    <table  class="table" id="total-individual-report" cellspacing="0" width="100%"></table>
      </div>
  </div>
 

</section>
<script src="js/allocation.js"></script>
<script src="js/edit-project-allocation.js"></script>
<script src="js/report.js"></script>
<?php
include_once 'footer.php';
?>