-- Task Tracker DB Alter script

	ALTER TABLE `tasks` ADD fk_userId INTEGER, ADD CONSTRAINT FOREIGN KEY(fk_userId) REFERENCES users(id);
	UPDATE tasks
		   JOIN users
		   ON tasks.userName = users.userName
	SET    tasks.fk_userId = users.id;


-- 21/11/2016

	ALTER TABLE `tasks` ADD fk_projectId INTEGER, ADD CONSTRAINT FOREIGN KEY(fk_projectId) REFERENCES projects(id);
	UPDATE tasks
		   JOIN projects
		   ON tasks.projectName = projects.projectName
	SET    tasks.fk_projectId = projects.id;

	ALTER TABLE `product_allocation` ADD fk_projectId INTEGER, ADD CONSTRAINT FOREIGN KEY(fk_projectId) REFERENCES projects(id);
	UPDATE product_allocation
		   JOIN projects
		   ON product_allocation.product = projects.projectName
	SET    product_allocation.fk_projectId = projects.id;

	ALTER TABLE `product_allocation` ADD fk_userId INTEGER, ADD CONSTRAINT FOREIGN KEY(fk_userId) REFERENCES users(id);
	UPDATE `product_allocation`
		   JOIN users
		   ON product_allocation.`Team-Member` = users.userName
	SET    product_allocation.fk_userId = users.id;

	ALTER TABLE `tasks` DROP `projectName`;
	ALTER TABLE `tasks` DROP `userName`;

-- 27/11/2016

	CREATE TABLE `designation` (
	  `id` int(11) NOT NULL,
	  `designation` varchar(255) NOT NULL,
	   PRIMARY KEY (id)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1;

	INSERT INTO `designation` (`id`, `designation`) VALUES
	(1, 'UI Developer'),
	(2, 'Sr. UI Developer'),
	(3, 'Sr. UI/UX Designer'),
	(4, 'UI/UX Designer'),
	(5, 'Creative Director');

	ALTER TABLE `users` ADD COLUMN fk_designationId INT NULL;
	ALTER TABLE `users` ADD CONSTRAINT FOREIGN KEY(fk_designationId) REFERENCES designation(id);

	UPDATE `users` AS U
	JOIN `designation` AS D
	ON D.designation = U.userDesignation
	SET U.fk_designationId = D.`id`;



	ALTER TABLE `product_allocation` ADD COLUMN fk_designationId INT NULL;
	ALTER TABLE `product_allocation` ADD CONSTRAINT FOREIGN KEY(fk_designationId) REFERENCES users(id);

	UPDATE `product_allocation` AS P
	JOIN `designation` AS D
	ON D.designation = P.Role
	SET P.fk_designationId = D.`id`;

	ALTER TABLE `product_allocation` DROP `Team-Member`;
	ALTER TABLE `product_allocation` DROP `Role`;
	ALTER TABLE `product_allocation` DROP `product`;


  --PRODUCT ALLOCATION TABLE

  --
  -- Table structure for table `product_allocation`
  --

  CREATE TABLE `product_allocation` (
    `id` int(11) NOT NULL,
    `Team-Member` varchar(100) NOT NULL,
    `Role` varchar(100) NOT NULL,
    `product` varchar(100) NOT NULL,
    `allocation` int(11) NOT NULL,
    `duration` varchar(50) NOT NULL
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;


  -- Indexes for table `product_allocation`
  --
  ALTER TABLE `product_allocation`
    ADD PRIMARY KEY (`id`);


    --
    -- Dumping data for table `product_allocation`
    --

    INSERT INTO `product_allocation` (`id`, `Team-Member`, `Role`, `product`, `allocation`, `duration`) VALUES
    (1, 'Arshmeet Singh', 'UI Developer', 'Green Job Interview', 50, '2016-11-14 - 2016-11-14'),
    (2, 'Amal', 'UI Developer', 'Green Job Interview', 100, '2016-11-14 - 2016-11-14'),
    (3, 'Amal', 'UI Developer', 'Money Haul', 6, '2016-11-12 - 2016-11-15'),
    (4, 'Amal', 'UI Developer', 'Medic Alert', 12, '2016-11-12 - 2016-11-15'),
    (5, 'Kundan Gupta', 'UI Developer', 'Med Circle', 25, '2016-11-22 - 2016-11-22'),
    (6, 'Kundan Gupta', 'UI Developer', 'Vatica Health', 50, '2016-11-23 - 2016-11-24'),
    (7, 'Kundan Gupta', 'UI Developer', 'Mpulse', 25, '2016-11-25 - 2016-11-25');




--Create Table Task Status <5 Dec 2016><DEEPAK>

CREATE TABLE `projectTimeTracker`.`task_status` ( `id` INT NOT NULL AUTO_INCREMENT , `status` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

INSERT INTO `task_status` (`id`, `status`) VALUES (NULL, 'Active'), (NULL, 'Done'), (NULL, 'Backlog')

--Create Table Task Phase <5 Dec 2016><DEEPAK>

CREATE TABLE `projectTimeTracker`.`task_phase` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `phase` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

INSERT INTO `task_phase` (`id`, `phase`) VALUES (NULL, 'Review & Analysis'), (NULL, 'UI Designing'), (NULL, 'UI Development'), (NULL, 'Testing')

--Alter Table Tasks, Add column for project status and project phase <6 Dec 2016><DEEPAK>

ALTER TABLE `tasks` ADD `fk_taskStatus` INT NOT NULL AFTER `fk_projectId`;
ALTER TABLE `tasks` ADD `fk_taskPhase` INT NOT NULL AFTER `fk_taskStatus`;

UPDATE tasks SET plannedHours = 0 where plannedHours = 0;
UPDATE tasks SET fk_taskStatus = 2 where fk_taskStatus = 0;
UPDATE tasks SET fk_taskPhase = 1 where fk_taskStatus = 0;
