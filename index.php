<?php
    include_once 'connection.php';
    include_once 'header.php';
    //echo $_SESSION['role'];
?>

<input type="hidden" value="<?php echo $logged_userId; ?>" id="user_id" />

<!-- content section -->
<section id="right-content" class="top-header">
  <header class="clearfix task-header">
    <div class="top-nav">
      <form id ="addtask" class="container-fluid">
        <div class="row">
        <div class="col-md-12">
      <ul class="list-inline add-project-details row">
        <li class="worked-on col-md-5 col-sm-4"><input name="txtDesc" id="txtDesc" class="task" type="text" placeholder="Task I worked on" /></li>
        <li class="block-two col-md-7 col-sm-8">
          <div class="row">
            <div class="select-project-wrapper col-md-4 col-sm-4">
              <div class="select-project">
                <!-- <img class="addproject-icon" src="images/addProject.svg"> -->

                <select data-size="7" id="projectName" class="selectpicker selectProject" data-live-search="true" title="Add Project"
                        data-live-search-placeholder="Search Project" data-size=5>
                    <?php
                      $getProjectQuery = "select * from projects order by projectName";
                      $exec = mysqli_query($connect, $getProjectQuery);
                      if($exec) {
                        while($row = mysqli_fetch_assoc($exec)) {
                          echo "<option project-id='".$row['id']."' data-tokens='".$row['projectName']."'>".$row['projectName']."</option>";
                        }
                      }
                    ?>
                </select>

              </div>
            </div>
            <div class="date-time large-screen col-md-6 col-sm-6">

              <div class="no-margin">
                <div class="time col-md-4 col-sm-4">
                  <span class="header-label">Hours</span>
                  <input type="text" value ="" placeholder="00" class="log-time hours" id="txtHours" />

                </div>
                <div class="time col-md-4 col-sm-4">
                  <span class="header-label">Minutes</span>
                  <input type="text" placeholder="00" value="" class="mins log-time" value="" name="txtMin" id="txtMin" />
                </div>
                <div class="time col-md-4 col-sm-4">
                  <span class="header-label">Date</span>
                  <input type="text"  placeholder="<?php echo $trimmedDate ?>" class="date datepicker" name="datepicker" id="datepicker" />
                </div>
              </div>

            </div>
            <div class="add-project col-md-2 col-sm-2">
              <button type="button" id="btnAdd" class="btn btn-default mod-button text-uppercase">add</button>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
    </form>
  </div>
  </header>


  <div class="content container-fluid">

    <!--ADD NEW PROJECT-->
    <div class="row">
      <div class="col-md-6">
        <span id="errorText"></span>
        <span id="successText"></span>
      </div>
      <div class="col-md-6 pull-right">
        <?php if($user_role == 1){ ?>
          <div class="row text-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#addproject">
              Add New Project
            </button>
            <button class="btn btn-info " data-toggle="modal" data-target="#addNewUser">
              Add New User
            </button>
          </div>
        <?php } ?>
      </div>
    </div>

    <!--ADD NEW PROJECT END-->

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <span id="hoursToday" class="hoursToday"></span>
      </div>
    </div>
    <ul id="tasklogToday" class="list-inline project-details row">
    </ul>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <span id="hoursYesterday" class="hoursToday"></span>
      </div>
    </div>
    <ul id="tasklogYesterday" class="list-inline project-details row">
    </ul>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <span class="hoursToday record">Older</span>
      </div>
    </div>
    <ul id="tasklog" class="list-inline project-details row">
    </ul>
  </div>

</section>


<div class="container">



  <div class="clearfix"></div>

  <!--REPORTS-->
  <div class="reports wrapper">
    <div class="col-sm-12">
      <h3 style="display:none;">Daily Task Report
        <div class="date-range-report">
          <input type="text" id="report-start-date" value="Start Date" class="form-control datepicker" name="datepicker" />
          <input type="text" id="report-end-date" value="End Date" class="form-control datepicker" name="datepicker" />
          <button type="button" class="btn btn-primary" id="btn-daterange-search">Search</button>
          <button type="button" class="btn btn-primary" id="btn-daterange-clear">Clear Search</button>
        </div>
      </h3>
    </div>
  <div class="clearfix"></div>
  </div>
  <!--REPORTS ENDS-->
</div>

<!--Edit Modal-->
<div class="modal fade modal-with-form" id="editTaskDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header blue-background">
          <button type="button" class="close" data-dismiss="modal" data-id="" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit Task Details</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="col-xs-12">
              <div class="form-group">
                <span>Description*</span>
                <textarea placeholder="Enter Description" rows="4" cols="50" class="form-control" value="" name="edittxtDesc" id="edittxtDesc"></textarea>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="form-group">
                <span>Project*</span>
                <select id="editProjectName" class="selectpicker form-control" data-live-search="true" data-size=5 size="8">
                  <?php
                    $getProjectQuery = "select * from projects order by projectName";
                    $exec = mysqli_query($connect, $getProjectQuery);
                    if($exec){
                      while($row = mysqli_fetch_assoc($exec)) {
                        echo "<option project-id='".$row['id']."' data-tokens='".$row['projectName']."'>".$row['projectName']."</option>";
                      }
                    }
                  ?>
                </select>
              </div>
            </div>
            <div class="col-xs-4">
              <div class="form-group">
                <span>Hours</span>
                <input type="text" value ="1" placeholder="0" class="form-control log-time" id="edittxtHours" />
              </div>
            </div>
            <div class="col-xs-4">
              <div class="form-group">
                <span>Min</span>
                <input type="text" placeholder="0" value="0" class="form-control log-time" value="" name="edittxtMin" id="edittxtMin" />
              </div>
            </div>
            <div class="col-xs-4">
              <div class="form-group">
                <span>Date*</span>
                <input type="text" readonly='true' placeholder="" class="form-control" id="editdatepicker" name="editdatepicker" />
              </div>
            </div>
            <div class="clearfix"></div>
          </form>
        </div>
        <div class="modal-footer">
          <p class="pull-left modalFormAlert"></p>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-default mod-save-button" id="update-button">Save changes</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Edit modal Ends-->

<!--DELETE Modal-->
<div class="modal fade" id="deleteTaskDetail" data-id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog modal-sm vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header blue-background">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Delete Task </h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <div class="confirmation-box">
                <p>
                  Are you sure you want to delete this data?
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" id="confirm-delete-data">Delete</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Delete modal Ends-->

<!--Add new project modal-->
<div class="modal fade" id="addproject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog modal-sm vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header blue-background">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Add New Project</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="form-group">
              <div class="col-md-12">
                <input placeholder="Add New Project" id="addProjectName" class="form-control" type="text" />
                <span id="newProjectAlert"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary" id="addProjectButton">Add</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Add new project modal ends -->

<!--Add new User modal-->
<div class="modal fade" id="addNewUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog modal-sm vertical-align-center" role="document">
      <div class="modal-content">
        <div class="modal-header blue-background">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Add New User</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <input id="newUserDesignation" class="form-control" />

              </div>

              <div class="form-group">
                <input placeholder="Name" id="newUserName" class="form-control" type="text" />

              </div>
              <div class="form-group">
                <input placeholder="Email" id="newUserEmail" class="form-control" type="email" />
              </div>

              <div class="form-group">
                <input placeholder="Password" id="newUserPword" class="form-control" type="password" />
              </div>


              <span id="newProjectAlert"></span>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary" id="addProjectButton">Add</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Add new User modal ends -->

<script src="js/taskLogControl.js"></script>
<script src="js/taskloglist.js"></script>



<script>
  jQuery(document).ready(function(){


    //show task log list
      showtasklog();

    /* submit task report to DB. */
    $('#btnAdd').click(function(){

      $('*').removeClass('alert-border');
      var userName = '<?php echo $logged_user; ?>';
      var projectName = $('button[data-id="projectName"]').attr('title');
      var timeHours = $('#txtHours').val();
      var timeMins = $('#txtMin').val();
      var timeDate = ($('#datepicker').val());

      if(!timeDate)
        {
          timeDate = $('#datepicker').attr('placeholder');
        }
      var taskDateFull = alterDateFormat(timeDate);

      var description = $('#txtDesc').val();
      var logged_user_id = $("input#user_id").val();
      var projectId = $('#projectName option:selected').attr('project-id');
      //console.log(projectId);
      //return false;

      if(!$('#txtDesc').val()){
        $('#txtDesc').addClass('alert-border');
        return false;
      }

      else if(projectName == "Add Project"){
        $('button[data-id="projectName"]').addClass('alert-border');
        return false;
      }

      else if(timeHours == 0 &&  timeMins == 0){
        $('#txtHours').addClass('alert-border');
        return false;
      }
      else if(timeHours > 24)
      {
        alert("Can't Enter hours more than 24");
        $('#txtHours').val("");
        return false;
      }
      else if(timeMins > 59)
      {
        alert("Can't Enter minutes more than 59");
        $('#txtMin').val("");
        return false;
      }
      $.ajax({
        url:'saveTask.php',
        type:'POST',
        data:{
                timeHours: timeHours,
                timeMins: timeMins,
                timeDate: taskDateFull,
                description: description,
                logged_user_id:logged_user_id,
                projectId: projectId
              },
        success:function(data){

          if(data == 1){
            showtasklog();
            ind_hours_current_month();
            $('#successText').text("Task Logged Successufully").fadeOut(2000);
          }
          else{
            //console.log(data);
            $('#errorText').text(data);
            $('.formAlert').removeClass('success-text').addClass('warning-text').text(data);
          }
        }
      });
    });




    /* ADD PROJECT FUNCTIONALITY */

    $('#addProjectName').on('focus',function(){
      $(this).css("border-color","#ccc");
      $('#addProjectName').val("");
    });

    $('#addProjectButton').on('click',function(){
      var new_project = $('#addProjectName').val();
      if(new_project == ""){
        $('#addProjectName').css("border-color","red");
        return false;
      }

      var createdOn = "<?php echo date("Y-m-d"); ?>";

      $.ajax({
        url:'addProject.php',
        type:'POST',
        data:{new_project:new_project,createdOn:createdOn},
        success:function(data){
          if(data === "1"){
            $('#addProjectName').val("");
            $('#newProjectAlert').text("Project Added Successfully").show();
          }
          else{
            $('#addProjectName').css("border-color","red");
            $('#newProjectAlert').text("Project Already Exists").show();
            return false;
          }
        }
      });
    });

    ind_hours_current_month();
  }); //JQuery document end
</script>

<?php include_once 'footer.php'; ?>
