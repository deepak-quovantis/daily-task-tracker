<?php
include_once 'connection.php';

$new_project = mysqli_real_escape_string($connect,$_POST['new_project']);
$createdOn   = mysqli_real_escape_string($connect,$_POST['createdOn']);

function seoUrl($string) {
    //Lower case everything
    $string = strtolower($string);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to no-space
    $string = preg_replace("/[\s_]/", "", $string);
    return $string;
}

$query = "select projectName from projects";
$exec = mysqli_query($connect, $query);
if($exec)
{

        $projects = array();
        while ($row = mysqli_fetch_assoc($exec)) {
                $convert_value = seoUrl($row['projectName']);
                $projects[] = $convert_value;
            }

   if (in_array(seoUrl($new_project), $projects))
    {

            echo "Project Already exists";
   }
        else{
            $query = "insert into projects(`projectName`,`createdOn`)
             values('".$new_project."','".$createdOn."')";
            $exec = mysqli_query($connect, $query);
            if($exec)
            {
                echo "1";
            }

        }



}

?>
