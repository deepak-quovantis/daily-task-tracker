<?php
  $now = date("Y-m-d");
  $trimmedDate = date("m/d");
  session_start();
  $pageName = basename($_SERVER['PHP_SELF']);
  //echo $pageName;
  $fileName = explode('.',$pageName)[0];
  if($fileName != 'login')
  {
    if(!isset($_SESSION['user_session']))
    {
      header("Location: login.php");
    }

    $logged_user = $_SESSION['user_session'];
    $user_role = $_SESSION['role'];
    $logged_user_email = $_SESSION['user_email'];
    $logged_userId = $_SESSION['user_id'];
  }

  //if not admin and try to see allocation page. Redirects to index.php
  if($fileName == 'project-allocation')
  {
    if($user_role != 1)
    {
      header("Location: index.php");
    }
  }

  //SET SESSION TIMEOUT DURATION
  $timeNow = $_SERVER['REQUEST_TIME'];
  $timeout_duration = 172800;

  if (isset($_SESSION["LAST_ACTIVITY"]) && ($timeNow - $_SESSION["LAST_ACTIVITY"]) > $timeout_duration) {
    session_unset();
    session_destroy();
    session_start();
  }

  $_SESSION["LAST_ACTIVITY"] = $timeNow;
?>
<!DOCTYPE html>
<html>
    <head>
       <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Daily Task Time Tracker</title>
        <!--FONTS-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">

        <!--CSS-->
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css"><!--jquery ui-->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!--Data Table CSS-->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">

        <!--fontAwesome-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/custom-style.css">
        <link rel="stylesheet" href="css/style2.css">
        <link rel="stylesheet" href="css/style.css">


        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">

        <!--SCRIPTS-->
        <script src="https://code.jquery.com/jquery-3.1.0.min.js"   integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s="   crossorigin="anonymous"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/task-manager.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>

        <!--data table JS-->
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

        <!--mustache js-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js"></script>

    </head>
    <body>
      <div class="wrapper">
        <!--Show sidebar except on login page-->
        <?php
          if($fileName != 'login'){
        ?>
            <div class="sidebar">
	         	<div class="logo"></div>

	         	<div class="list">
    	   			<div class="profile-pic"><img src="images/blankimg.png" /></div>
    	   			<span class="profile-text"><?php echo $logged_user ?></span>
    	   		</div>

	        	<ul class="nav nav-pills nav-stacked side-list">
	        	   	<li>
	        	   		<a href="index.php" class="active list">
	        	   			<img class="nav-icons" src="images/dashboard.svg" />
	        	   			<span class="side-text">dashboard</span>
	        	   		</a>
	        	   	</li>
	        	    <li>
	        	    	<a href="reports.php" class="list">
	        	    		<img class="nav-icons" src="images/report.svg" />
	        	    		<span class="side-text">reports</span>
	        	    	</a>
	        	    </li>
                <?php if($user_role == 1){ ?>
                  <li>
  	        	   		<a href="project-allocation.php"class="list">
  	        	   			<img class="nav-icons" src="images/projects.svg" />
  	        	   			<span class="side-text">projects</span>
  	        	   		</a>
  	        	   	</li>
                <?php } ?>
	        	   	<!--<li>
	        	   		<a href="#" data-toggle="pill" class="list">
	        	   			<span class="notification">2</span>
	        	   			<img class="nav-icons" src="images/notifications.svg" />
	        	   			<span class="side-text">notifications</span>
	        	   		</a>
	        	   	</li> -->
	        	   	<li>
	        	   		<a href="logout.php" class="list">
	        	   			<img class="nav-icons" src="images/logout.svg" />
	        	   			<span class="side-text">logout</span>
	        	   		</a>
	        	   	</li>
	        	</ul>
            <p id="myHours">
              Hours This Month
              <span></span>
            </p>
	      	</div>
        <?php  } ?>
